{{-- リストマスターレイアウトを継承 --}}
@extends('master.list')

{{-- CSSの定義 --}}
@section('css')
@parent
@stop

{{-- JSの定義 --}}
@section('js')
@parent
<script type="text/javascript">
$(function(){
    // セッションがある時に更新ダイアログを表示
    @if(session('update', 0))
        SliderBox.info('更新が完了しました。');
        {{ session(['update' => null]) }}
    @endif
});
</script>
@stop

{{-- メイン部分の呼び出し --}}
@section('content')

<div class="row">
    <div class="col col-sm-8 col-sm-offset-2">
    
        {{-- タイトル --}}
        <h5 class="mb30"><i class="fa fa-th-list"></i> {{ $title }}一覧</h5>
        
        {{-- エラーメッセージ --}}
        @include('_errors.list')
        
        {{-- アナウンス --}}
        <p>
            <small>ファイルをアップロードしてください</small>
        </p>
        
        {{-- csvファイルのパスの指定とアップロード --}}
        @include( 'master.csv.upload', ['sampleUrl' =>  asset('csv/new/medley.csv'), 'file_type' => 'csv_medley', 'sampleText' => 'メドフェスサンプルCSV'] )
    
    </div><!-- .col -->
</div><!-- .row -->

@stop
