
<ul class="nav nav-tabs main-nav-tabs">
	<li role="presentation" class="{{ Request::is('top/list/*') ? 'active' : '' }}">
		<a href="{{ url('app/topics/list.php') }}"><i class="fa fa-list-alt"></i> トピックス</a>
	</li>
	<li role="presentation" class="{{ Request::is('top/car_select/*') ? 'active' : '' }}">
		<a href="{{ url('app/topics_set/car.php?selectFlg=True') }}"><i class="fa fa-list-alt"></i> 車種&nbsp;紐づけ</a>
	</li>
	<li role="presentation" class="{{ Request::is('top/hansha_select/*') ? 'active' : '' }}">
		<a href="{{ url('app/topics_set/hansha.php?selectFlg=True') }}"><i class="fa fa-list-alt"></i> 販社&nbsp;紐づけ</a>
	</li>
	<li role="presentation" class="{{ Request::is('top/car_search/*') ? 'active' : '' }}">
		<a href="{{ url('app/topics_set/car.php') }}"><i class="fa fa-list-alt"></i> 車種&nbsp;検索</a>
	</li>
	<li role="presentation" class="{{ Request::is('top/hansha_search/*') ? 'active' : '' }}">
		<a href="{{ url('app/topics_set/hansha.php') }}"><i class="fa fa-list-alt"></i> 販社&nbsp;検索</a>
	</li>
	<li role="presentation" class="{{ Request::is('top/delete/*') ? 'active' : '' }}">
		<a href="{{ url('app/topics_del/list.php') }}"><i class="fa fa-list-alt"></i> 削除済み</a>
	</li>
</ul>
