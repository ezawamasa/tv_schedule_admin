{{-- カテゴリーレイアウトを継承 --}}
@extends('master._master')

{{-- CSSの定義 --}}
@section('css')
@parent
@stop

{{-- JSの定義 --}}
@section('js')
@parent
	<script>
		var eventData = []
		$(function () {
			eventData = $( 'input[name="event_data"]' ).val();

			$('#calendar').fullCalendar({
				editable: true, // イベントを編集するか
				allDaySlot: false, // 終日表示の枠を表示するか

				eventDurationEditable: false, // イベント期間をドラッグしで変更するかどうか

				slotEventOverlap: false, // イベントを重ねて表示するか

				selectable: true,

				selectHelper: true,

				// 日の枠内を選択したときの処理
				select: function(start, end, allDay) {

					console.log( 11111 );

				},

				// イベントをクリックしたときの処理
				eventClick: function(calEvent, jsEvent, view) {

					
					console.log( 22222 );

					console.log( eventData );

				},

				droppable: true,// イベントをドラッグできるかどうか

				
				
				// 登録されているイベントの一覧
				events: [

					{

						'title': 'イベント',

						'start': '2018-12-14 00:00:00',
						'end' : '2018-12-15 12:00:00'

					},
					{

						title: 'イベント',

						start: '2018-12-15'

					}

				]
			});
		});
	</script>
@stop

{{-- タブ部分の定義 --}}
@section('tabs')
	@include('top._tabs')
@stop

{{-- メイン部分の呼び出し --}}
@section('content')
<div class="row">
	
	<div id="main" class="col-sm-12">
		
		<h5 class="mb30"><i class="fa fa-th-list"></i> {{ $title }}</h5>

		<div class="panel panel-default col-md-12" style="padding: 20px 20px 20px 20px; height: 1200px;">


			<div class="row">
				{{-- TV番組入力 --}}
				<div class="col col-sm-4 col-md-4">
					<a href="{{ url( 'tv/tv_reserve/create' ) }}" class="top_link">
						»&nbsp;TV番組入力
					</a>
				</div>

				{{-- TV番組一覧 --}}
				<div class="col col-sm-4 col-md-4">
					<a href="{{ url( 'tv/tv_reserve/list' ) }}" class="top_link">
						»&nbsp;TV番組一覧
					</a>
				</div>
			</div>

			<div class="row">
				{{-- イベント入力 --}}
				<div class="col col-sm-4 col-md-4">
					<a href="{{ url( 'event/create' ) }}" class="top_link">
						»&nbsp;イベント入力
					</a>
				</div>

				{{-- イベント一覧 --}}
				<div class="col col-sm-4 col-md-4">
					<a href="{{ url( 'event/list' ) }}" class="top_link">
						»&nbsp;イベント一覧
					</a>
				</div>
			</div>

			{{-- calenderを表示する --}}
			<div id="calendar"></div>

			<?php
			// イベントの配列をJSONに変換する
			$eventJson = json_encode( $eventList );
			?>

			{{-- イベントのデータをhiddenに保持する --}}
			{!! Form::hidden( 'event_data', $eventJson ) !!}

		</div>{{-- panel --}}

	</div>{{-- main --}}
</div>{{-- row --}}
@stop