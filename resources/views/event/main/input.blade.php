
{{-- カテゴリーレイアウトを継承 --}}
@extends('master.input')

{{-- JSの定義 --}}
@section('js')
@parent
@stop

{{-- メニューの読み込み --}}
@section("tabs")
@include( 'other._tabs' )
@stop

{{-- 入力内容 --}}
@section("input")

{{-- 追加と編集の時で処理を分ける --}}
@if( $type == "edit" )
    {{-- 編集の時の処理 --}}
    {!! Form::model(
        $targetMObj,
        ['id'=> 'regEditForm', 'method' => 'PUT', 'url' => action( $displayObj->ctl . '@putEdit', ['id' => $targetMObj->id] ), 'enctype' => 'multipart/form-data']
    ) !!}

@else
    {{-- 確認画面に遷移 --}}
    {!! Form::model(
        $targetMObj,
        ['id'=> 'regEditForm', 'method' => 'PUT', 'url' => action( $displayObj->ctl . '@putCreate' ), 'enctype' => 'multipart/form-data']
    ) !!}

@endif

<div class="row">
    <div class="panel panel-default">
        <table class="table table-bordered tbl-txt-center tbl-input-line">
            <tbody>
                {{-- カテゴリー --}}
                <tr>
                    <th class="bg-primary">カテゴリー <span class="color-dpink">※</span></th>
                    <td>
                        @include( 'elements.event.category_select', ['id' => 'category', 'options' => ['class' => 'form-control']] )
                    </td>
                </tr>

                {{-- イベント名 --}}
                <tr>
                    <th class="bg-primary">イベント名 <span class="color-dpink">※</span></th>
                    <td>
                        {!! Form::text( 'name', null, ['class' => 'form-control'] ) !!}
                    </td>
                </tr>
                
                {{-- 開始日時 --}}
                <tr>
                    <th class="bg-primary"> 開始日時 <span class="color-dpink">※</span></th>
                    <td>
                        <div class="row">
                            <div class="col col-sm-6">
                                {!! Form::text( 'start_day', null, ['class' => 'form-control datepicker', 'placeholder' => '日付を入力してください'] ) !!}
                            </div>
                            <div class="col col-sm-6">
                                {!! Form::text( 'start_time', null, ['class' => 'form-control clockpicker', 'placeholder' => '時間を入力してください'] ) !!}
                            </div>
                        </div>
                    </td>
                </tr>
                
                {{-- 終了日時 --}}
                <tr>
                    <th class="bg-primary"> 終了日時 <span class="color-dpink">※</span></th>
                    <td>
                        <div class="row">
                            <div class="col col-sm-6">
                                {!! Form::text( 'end_day', null, ['class' => 'form-control datepicker', 'placeholder' => '日付を入力してください'] ) !!}
                            </div>
                            <div class="col col-sm-6">
                                {!! Form::text( 'end_time', null, ['class' => 'form-control clockpicker', 'placeholder' => '時間を入力してください'] ) !!}
                            </div>
                        </div>
                    </td>
                </tr>

                {{-- 備考 --}}
                <tr>
                    <th class="bg-primary">備考</th>
                    <td>
                        {!! Form::textarea( 'memo', null, ['class' => 'form-control'] ) !!}
                    </td>
                </tr>

            </tbody>
        </table>
    </div>
</div>

<div class="row">
    {{-- 戻るボタン --}}
    <div class="col-sm-2">
        <button type="button" onClick="location.href ='{{ action( $displayObj->ctl . '@getIndex') }}'" class="btn btn-warning btn-block btn-embossed">
            <i class="fa fa-mail-reply"></i> 戻る
        </button>
    </div>

    {{-- 確認画面 --}}
    <div class="col-sm-4 col-sm-offset-2">
        {!! Form::submit( $type == "edit" ? '編集' : '登録', ['id' => $buttonId, 'class' => 'btn btn-info btn-block btn-embossed']) !!}
    </div>

    <div class="col-sm-2">
    </div>
</div>

{!! Form::close() !!}

@stop
