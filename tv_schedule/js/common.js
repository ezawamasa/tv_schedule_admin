$( function() {
    // 日付のカレンダー入力UIの設定
    $( '.datepicker' ).datepicker({
        dateFormat:'yy-mm-dd', // 日付のフォーマット
        autoclose: true, // 入力後自動で閉じる。
        language: 'ja' // 日本語
    });
    
    // 時刻の入力UIの設定
    $('.clockpicker').clockpicker({
        autoclose: true, // 入力後自動で閉じる
    });
});