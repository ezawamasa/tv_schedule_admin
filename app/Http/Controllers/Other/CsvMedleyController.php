<?php

namespace App\Http\Controllers\Other;

use App\Commands\Csv\CsvUploadCommand;
use App\Http\Requests\CsvUploadRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\tInitSearch;
use Session;

/**
 * メドフェスデータアップロード画面用コントローラー
 *
 * @author 江澤
 *
 */
class CsvMedleyController extends Controller{

    use tInitSearch;
    
    /**
     * コンストラクタ
     */
    public function __construct(){
        // 表示部分で使うオブジェクトを作成
        $this->initDisplayObj();
    }

    #######################
    ## initalize
    #######################

    /**
     * 表示部分で使うオブジェクトを作成
     * @return [type] [description]
     */
    public function initDisplayObj(){
        // 表示部分で使うオブジェクトを作成
        $this->displayObj = app('stdClass');
        // カテゴリー名
        $this->displayObj->category = "other";
        // 画面名
        $this->displayObj->page = "csvMedley";
        // 基本のテンプレート
        $this->displayObj->tpl = $this->displayObj->category . "." . $this->displayObj->page;
        // コントローラー名
        $this->displayObj->ctl = "Other\CsvMedleyController";
    }
    
    #######################
    ## 検索・並び替え
    #######################

    /**
     * 画面固有の初期条件設定
     *
     * @return array
     */
    public function extendSearchParams(){
        $search = [];

        return $search;
    }
    
    #######################
    ## Controller method
    #######################
    
    /**
     * 一覧画面のデータを表示
     * @param  [type] $search     [description]
     * @param  [type] $requestObj [description]
     * @return [type]             [description]
     */
    public function showListData( $search, $sort, $requestObj ){
        return view(
            $this->displayObj->tpl . '.index'
        )
        ->with( 'displayObj', $this->displayObj )
        ->with( 'title', "メドフェスデータアップロード" );
    }
    
    /**
     * CSVファイルアップロード
     * @param  CsvUploadRequest $requestObj [description]
     * @return [type]                       [description]
     */
    public function postUpload( CsvUploadRequest $requestObj ){
        // CSVアップロードの時間を上げる
        set_time_limit(18432);
        // メモリの上限を変更
        ini_set('memory_limit', '2048M');
        
        #try{
            
            $csv = $this->dispatch(
                new CsvUploadCommand(
                    $requestObj
                )
            );
            
            $csverrors = "";
            $info = "";
            
            if( $csv->result->hasError() ) {
                //
                $csverrors = $csv->result->errors();
                
            } else {
                $info = "正常に終了しました";
            }
            
        #} catch ( \Exception $e ) {
        #   \Log::debug( $e );
        #}
        
        // エラーのない時はアップロード画面に戻る
        if( empty( $csverrors ) == True ){
            // 更新のセッションを用意
            Session::put('update', 1);

            return redirect( action( $this->displayObj->ctl . '@getList' ) );

        }else{
            return view(
                $this->displayObj->tpl . '.index',
                compact(
                    'csverrors',
                    'info'
                )
            )
            ->with( 'displayObj', $this->displayObj )
            ->with( 'title', "メドフェスデータアップロード" );
            
        }

    }

}
