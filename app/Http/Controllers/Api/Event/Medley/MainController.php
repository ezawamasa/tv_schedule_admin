<?php

namespace App\Http\Controllers\Api\Event\Medley;

use App\Commands\Event\Medley\Main\ListCommand;
use App\Models\Event\Medley;
use App\Http\Requests\SearchRequest;
use App\Http\Requests\Event\MedleyRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\tInitSearch;

/**
 * メドレーフェスティバル画面用コントローラー
 *
 * @author 江澤
 *
 */
class MainController extends Controller{
    
    use tInitSearch;
    
    /**
     * コンストラクタ
     */
    public function __construct(){
        // 表示部分で使うオブジェクトを作成
        $this->initDisplayObj();
    }

    #######################
    ## initalize
    #######################

    /**
     * 表示部分で使うオブジェクトを作成
     * @return [type] [description]
     */
    public function initDisplayObj(){
        // 表示部分で使うオブジェクトを作成
        $this->displayObj = app('stdClass');
        // カテゴリー名
        $this->displayObj->category = "event";
        // サブカテゴリー名
        $this->displayObj->subCategory = "medley";
        // 画面名
        $this->displayObj->page = "main";
        // 基本のテンプレート
        $this->displayObj->tpl = $this->displayObj->category . "." . $this->displayObj->subCategory . "." . $this->displayObj->page;
        // コントローラー名
        $this->displayObj->ctl = "Event\Medley\MainController";

        // 出力するcsvファイル名
        $this->displayObj->csvFileName = 'medley.csv';
    }

    #######################
    ## 検索・並び替え
    #######################

    /**
     * 画面固有の初期条件設定
     *
     * @return array
     */
    public function extendSearchParams(){
        $search = [];
        
        return $search;
    }

    /**
     * 並び順のデフォルト値を指定
     * ※継承先で主に定義
     * @return [type] [description]
     */
    public function extendSortParams() {
        // 複数テーブルにあるidが重複するため明示的にエイリアス指定
        $sort = [ 'tb_event_medley.created_at' => 'asc'];
        
        return $sort;
    }

    #######################
    ## Controller method
    #######################

    /**
     * 一覧画面のデータを表示
     * @param  [type] $search     [description]
     * @param  [type] $requestObj [description]
     * @return [type]             [description]
     */
    public function showListData( $search, $sort, $requestObj ){
        
        // 表示データを取得
        $showData = $this->dispatch(
            new ListCommand(
                $sort,
                $requestObj,
                "api"
            )
        );

        //　表示用に、並び替え情報を取得
        if( isset( $sort['sort'] ) == True && !empty( $sort['sort'] ) == True ){
            foreach ( $sort['sort'] as $key => $value ) {
                // 並び替え情報を格納
                $sortTypes = [
                    'sort_key' => $key,
                    "sort_by" => $value
                ];
            }
        }

        // 表示確認用の時
        if( !empty( $search['show'] ) == True && $search['show'] == "1" ){

            foreach( $showData as $key => $value ){
                
                //dd($value);                
                echo "id " . $value->id . "<br>";
                echo "event_name " . $value->event_name;
                // 配列の最後でないとき
                if( end( $showData ) != $value ){
                    echo "<br>";
                }

            }
        }else{

            /**
             * データをJSON形式にして出力する
             * @param  string|array  $data 出力させるデータ
             * @param  int  $status ステータスコード
             * @param  array  $headers
             * @param  int  $options
             * @return \Illuminate\Http\JsonResponse
             */
            return Response()
                        ->json( 
                            $showData,
                            200,
                            [],
                            JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_QUOT | JSON_HEX_AMP | JSON_UNESCAPED_UNICODE
                        );
        }

    }
}
