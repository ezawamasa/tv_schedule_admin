<?php

namespace App\Http\Controllers\Top;

use App\Lib\Util\DateUtil;
use App\Original\Util\SessionUtil;
use App\Commands\Event\ListCommand;
use App\Http\Requests\SearchRequest;
use App\Http\Controllers\Controller;
use App\Http\Controllers\tInitSearch;
use App\Models\Role;

/**
 * TOP画面用コントローラー
 *
 * @author 江澤
 *
 */
class TopController extends Controller{
    
    use tInitSearch;

    /**
     * コンストラクタ
     */
    public function __construct(){
        // 表示部分で使うオブジェクトを作成
        $this->initDisplayObj();
        
        // top画面を開いた時は検索項目は消去
        SessionUtil::removeSearch();
    }

    #######################
    ## initalize
    #######################

    /**
     * 表示部分で使うオブジェクトを作成
     * @return [type] [description]
     */
    public function initDisplayObj(){
        // 表示部分で使うオブジェクトを作成
        $this->displayObj = app('stdClass');
        // カテゴリー名
        $this->displayObj->category = "top";
        // 画面名
        $this->displayObj->page = "index";
        // 基本のテンプレート
        $this->displayObj->tpl = $this->displayObj->category . "." . $this->displayObj->page;
        // コントローラー名
        $this->displayObj->ctl = "Top\TopController";
    }

    #######################
    ## 検索・並び替え
    #######################

    /**
     * 画面固有の初期条件設定
     *
     * @return array
     */
    public function extendSearchParams(){
        $search = [];

        return $search;
    }

    #######################
    ## Controller method
    #######################
    
    /**
     * 一覧画面のデータを表示
     * @param  [type] $search     [description]
     * @param  [type] $requestObj [description]
     * @return [type]             [description]
     */
    public function showListData( $search, $sort, $requestObj ){
        $eventList = [];
        // 表示データを取得
        $eventData = $this->dispatch(
            new ListCommand(
                $sort,
                $requestObj
            )
        );
        
        // DBから取得したデータを配列に格納する
        foreach( $eventData as $value ){
            // イベント名
            $eventList['title'] = $value['name'];
            // 開始日時
            $eventList['start'] = date( "Y-m-d H:i:s", strtotime( $value['start_date'] ) );
            // 終了日時
            $eventList['end'] = date( "Y-m-d H:i:s", strtotime( $value['end_date'] ) );
        }

        

        return view(
            $this->displayObj->tpl,
            compact(
                'search',
                'eventList'
            )
        )
        ->with( 'displayObj', $this->displayObj )
        ->with( 'title', "メニューインデックス" );
    }

}
