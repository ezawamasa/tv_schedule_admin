<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

// イベント画面のコントローラー
Route::group( ['prefix' => 'event'], function() {
    // メドフェス画面のコントローラ
    Route::group( ['prefix' => 'medley'], function() {
        Route::get( 'search',      'Api\Event\Medley\MainController@getList' ); // メドフェス
        Route::get( 'sort',        'Api\Event\Medley\MainController@getSort' ); // メドフェス
    });
});